import webbrowser

class Movie():
	def __init__(self, title, desc, poster_url, trailer_url):
		self.title = title
		self.desc = desc
		self.poster_url = poster_url
		self.trailer_url = trailer_url

	def play_trailer(self):
		webbrowser.open(self.trailer_url)
