#!/usr/bin/env python

import sys
from pickle import dump
import tmdb3
from subprocess import call

#import imdb

from media import Movie

def add_movie(title):
	if len(sys.argv) < 2:
		print('usage: ' + sys.argv[0] + ' <title_of_movie>')
		sys.exit(1)

#	i_conn = imdb.IMDb()

#	movie_id = i_conn.search_movie(sys.argv[1])[0].getID()
#	search_res = i_conn.get_movie(movie_id)
#	i_conn.update(search_res)

#	title = search_res['title']
#	desc = search_res['plot']
#	desc = desc[len(desc) - 1]
#	cover_url = search_res['full-size cover url']
	
#	with open('api_key', 'r') as f:
#		hex_key = f.readline()
#		print(hex_key)
#		tmdb3.set_key(hex_key)

	tmdb3.set_key('23afca60ebf72f8d88cdcae2c4f31866')
	tmdb_movie = tmdb3.searchMovie(sys.argv[1])[0]

	title = tmdb_movie.title
	desc = tmdb_movie.overview	
	trailer_url = tmdb_movie.youtube_trailers[0].geturl()
	cover_url = tmdb_movie.poster.geturl('w342')
	
	movie = Movie(title, desc, cover_url, trailer_url)

	with open('my_movie_db', 'a') as f:
		dump(movie, f)

add_movie(sys.argv)
