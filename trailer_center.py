#!/usr/bin/env python

import pickle

import fresh_tomatoes

import media

movies = []
with open('my_movie_db', 'r') as f:
	while True:
		try:
			movies.append(pickle.load(f))
		except (EOFError, pickle.UnpicklingError):
			break

fresh_tomatoes.open_movies_page(movies)
