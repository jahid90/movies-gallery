#!/usr/bin/env python

import sys
import imdb

import media

def add_movie(title):
	if len(sys.argv) < 2:
		print('usage: ' + sys.argv[0] + ' <title_of_movie>')
		sys.exit(1)

	i_conn = imdb.IMDb()

	movie_id = i_conn.search_movie(sys.argv[1])[0].getID()
	search_res = i_conn.get_movie(movie_id)
	i_conn.update(search_res)

	title = search_res['title']
	desc = search_res['plot']
	cover_url = search_res['cover url']
	trailer_url = ''
	movie = media.Movie(title, desc, cover_url, trailer_url)

	print(movie.desc[len(movie.desc) - 1].encode('ascii', 'ignore'))

add_movie(sys.argv)
