#!/usr/bin/env python

from subprocess import call
import pickle

call(['rm', 'my_movie_db'])

with open('my_list', 'r') as f:
	for line in f:
		print('adding ' + line.rstrip())
		call(['./add_movie.py', line])
