#!/usr/bin/python

from imdb import IMDb

imdb_conn = IMDb('http')

interstellar = imdb_conn.search_movie('interstellar')[0]
imdb_conn.update(interstellar)

print(interstellar['title'].encode('ascii', 'ignore'), interstellar['plot'], interstellar['cover url'])
